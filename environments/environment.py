import pygame

from typing import Tuple
from config import COLOR


class Environment:

    def __init__(self, num_actions) -> None:
        self.num_actions = num_actions

    def step(self, action_ind: int) -> Tuple[float, list, bool]:
        return (0, [0], False)

    def render(self, screen: pygame.Surface) -> None:
        screen.fill(COLOR.BLACK)
