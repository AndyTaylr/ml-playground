import pygame
import random

from typing import Tuple, Dict, Any, Optional
from enum import Enum, auto

from .environment import Environment
from config import SCREEN_WIDTH, SCREEN_HEIGHT, COLOR


class Actions(Enum):
    MOVE_UP = auto()
    MOVE_DOWN = auto()
    MOVE_LEFT = auto()
    MOVE_RIGHT = auto()


class Creep:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    def render(self, screen: pygame.Surface, agent: bool, tile_to_screen: Any):
        r_x, r_y = tile_to_screen(self.x + 0.5, self.y + 0.5)  # since circle draws at center
        tile_width = tile_to_screen(1, 1)[0]
        radius = int(tile_width * 0.3)

        if agent:
            pygame.draw.circle(screen, COLOR.WHITE, (r_x, r_y), radius)
            pygame.draw.circle(screen, COLOR.BLACK, (r_x, r_y), int(radius * 0.5))
        else:
            pygame.draw.circle(screen, COLOR.ORANGE, (r_x, r_y), radius)

            stripe_width = radius * 2 // 5
            pygame.draw.rect(screen, COLOR.BLACK, (r_x - radius + stripe_width, r_y - radius, stripe_width, radius * 2))
            pygame.draw.rect(screen, COLOR.BLACK, 
                             (r_x + radius - stripe_width * 2, r_y - radius, stripe_width, radius * 2))

    def get_pos(self) -> Tuple[int, int]:
        return (self.x, self.y)


class Screeps(Environment):
    def __init__(self, custom_settings=None) -> None:
        super().__init__(4)

        self.load_settings(custom_settings)
        self.agent_creep = self.create_creep()

        self.chaser_creep = self.create_creep()
        while self.chaser_creep.get_pos() == self.agent_creep.get_pos():
            self.chaser_creep = self.create_creep()

    def step(self, action_ind: int) -> Tuple[float, list, bool]:
        assert action_ind < len(list(Actions)) and action_ind >= 0

        action = list(Actions)[action_ind]

        self.handle_action(action)
        self.move_chaser()

        return (0.0, [], False)

    def handle_action(self, action, creep: Optional[Creep]=None) -> None:
        if creep is None:
            creep = self.agent_creep

        if action == Actions.MOVE_DOWN:
            creep.y += 1
        elif action == Actions.MOVE_UP:
            creep.y -= 1
        elif action == Actions.MOVE_LEFT:
            creep.x -= 1
        elif action == Actions.MOVE_RIGHT:
            creep.x += 1

    def move_chaser(self) -> None:
        x, y = self.chaser_creep.get_pos()
        t_x, t_y = self.agent_creep.get_pos()

        possible_moves = []

        if t_x < x:
            possible_moves.append(Actions.MOVE_LEFT)
        elif t_x > x:
            possible_moves.append(Actions.MOVE_RIGHT)

        if t_y < y:
            possible_moves.append(Actions.MOVE_UP)
        elif t_y > y:
            possible_moves.append(Actions.MOVE_DOWN)

        if possible_moves:  # Make state transition stochastic
            self.handle_action(random.choice(possible_moves), self.chaser_creep)

    def render(self, screen: pygame.Surface) -> None:
        self.render_tiles(screen)

        self.agent_creep.render(screen, True, self.tile_to_screen)
        self.chaser_creep.render(screen, False, self.tile_to_screen)

    def render_tiles(self, screen: pygame.Surface) -> None:
        for x in range(self.settings['TILE_WIDTH'] + 1):
            pygame.draw.rect(screen, COLOR.WHITE, (self.tile_to_screen(x, 0)[0] - 2, 0, 4, SCREEN_HEIGHT))

        for y in range(self.settings['TILE_HEIGHT'] + 1):
            pygame.draw.rect(screen, COLOR.WHITE, (0, self.tile_to_screen(0, y)[1] - 2, SCREEN_WIDTH, 4))

    def create_creep(self) -> Creep:
        start_x = random.randint(0, self.settings['TILE_WIDTH'] - 1)
        start_y = random.randint(0, self.settings['TILE_HEIGHT'] - 1)

        return Creep(start_x, start_y)

    def tile_to_screen(self, x: float, y: float) -> Tuple[int, int]:
        tile_width, tile_height = self.get_tile_size()

        return (int(x * tile_width), int(y * tile_height))

    def get_tile_size(self) -> Tuple[float, float]:
        width = SCREEN_WIDTH / self.settings['TILE_WIDTH']
        height = SCREEN_HEIGHT / self.settings['TILE_HEIGHT']

        return (width, height)

    def load_settings(self, custom_settings) -> None:
        self.settings: Dict[str, Any] = {
            'TILE_WIDTH': 5,
            'TILE_HEIGHT': 5
        }

        if custom_settings is not None:
            for key, val in custom_settings.items():
                if key in self.settings.keys():
                    self.settings[key] = val
