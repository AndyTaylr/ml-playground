import pygame
from .environment import Environment

from enum import Enum, auto
from typing import Tuple, Optional, Dict, Any

from config import COLOR, SCREEN_WIDTH, SCREEN_HEIGHT


class Actions(Enum):
    MOVE_UP = auto()
    MOVE_DOWN = auto()
    MOVE_LEFT = auto()
    MOVE_RIGHT = auto()


class Pos:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    def move(self, action: Actions):
        if action == Actions.MOVE_DOWN:
            self.y += 1
        elif action == Actions.MOVE_UP:
            self.y -= 1
        elif action == Actions.MOVE_LEFT:
            self.x -= 1
        elif action == Actions.MOVE_RIGHT:
            self.x += 1

    def bound(self, min_x: int, min_y: int, max_x: int, max_y: int) -> bool:
        # Returns whether or not the pos was OOB
        # Upper bound is exclusive
        prev_x, prev_y = self.get_pos()

        self.x = min(max_x - 1, max(min_x, self.x))
        self.y = min(max_y - 1, max(min_y, self.y))

        return self.get_pos != (prev_x, prev_y)

    def get_pos(self) -> Tuple[int, int]:
        return (self.x, self.y)


class GridWorld(Environment):

    def __init__(self, custom_settings:Optional[Dict]=None) -> None:
        super().__init__(4)

        self.load_settings(custom_settings)

        self.reset()

    def step(self, action_ind: int) -> Tuple[float, list, bool]:
        action = list(Actions)[action_ind]

        done = False
        if self.agent_on_goal():
            done = True

        self.agent_pos.move(action)
        self.agent_pos.bound(0, 0, self.settings['TILE_WIDTH'], self.settings['TILE_HEIGHT'])

        reward = 0
        if self.agent_on_goal():
            reward = 1

        return (reward, [self.agent_pos.x, self.agent_pos.y], done)

    def render(self, screen: pygame.Surface) -> None:
        self.render_grid(screen)
        self.render_agent(screen)
        self.render_goal(screen)

    def reset(self) -> None:
        self.agent_pos: Pos = Pos(self.settings['START_X'], self.settings['START_Y'])
        self.goal_pos: Pos = Pos(self.settings['GOAL_X'], self.settings['GOAL_Y'])

    def render_agent(self, screen: pygame.Surface) -> None:
        r_x, r_y = self.tile_to_screen(self.agent_pos.x + 0.5, self.agent_pos.y + 0.5)  # since circle draws at center
        tile_width = self.tile_to_screen(1, 1)[0]
        radius = int(tile_width * 0.2)

        pygame.draw.circle(screen, COLOR.WHITE, (r_x, r_y), radius)

    def render_goal(self, screen: pygame.Surface) -> None:
        margin = 1  # pixel

        r_x, r_y = self.pos_to_screen(self.goal_pos)
        tile_w, tile_h = self.get_tile_size(True)

        pygame.draw.rect(screen, COLOR.GREEN, (r_x + margin, r_y + margin, tile_w - margin * 2, tile_h - margin * 2))

    def render_grid(self, screen: pygame.Surface) -> None:
        for x in range(self.settings['TILE_WIDTH'] + 1):
            pygame.draw.rect(screen, COLOR.WHITE, (self.tile_to_screen(x, 0)[0] - 2, 0, 4, SCREEN_HEIGHT))

        for y in range(self.settings['TILE_HEIGHT'] + 1):
            pygame.draw.rect(screen, COLOR.WHITE, (0, self.tile_to_screen(0, y)[1] - 2, SCREEN_WIDTH, 4))

    def agent_on_goal(self) -> bool:
        return self.agent_pos.get_pos() == self.goal_pos.get_pos()

    def pos_to_screen(self, pos: Pos) -> Tuple[int, int]:
        return self.tile_to_screen(pos.x, pos.y)

    def tile_to_screen(self, x: float, y: float) -> Tuple[int, int]:
        tile_width, tile_height = self.get_tile_size()

        return (int(x * tile_width), int(y * tile_height))

    def get_tile_size(self, cast: bool=False) -> Tuple[float, float]:
        width = SCREEN_WIDTH / self.settings['TILE_WIDTH']
        height = SCREEN_HEIGHT / self.settings['TILE_HEIGHT']

        if cast:
            return (int(width), int(height))
        else:
            return (width, height)

    def load_settings(self, custom_settings: Optional[Dict]) -> None:
        self.settings: Dict[str, Any] = {
            'START_X': 0,
            'START_Y': 0,
            'GOAL_X': 4,
            'GOAL_Y': 2,
            'TILE_WIDTH': 5,
            'TILE_HEIGHT': 5
        }

        if custom_settings is not None:
            for key, val in custom_settings.items():
                if key in self.settings.keys():
                    self.settings[key] = val
