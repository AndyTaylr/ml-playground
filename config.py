
# Global Graphics
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 800


# Colours
class _ColorClass:
    WHITE = (255, 255, 255)
    BLACK = (0, 0, 0)
    YELLOW = (255, 255, 0)
    BLUE = (0, 0, 255)
    ORANGE = (255, 165, 0)
    GREEN = (0, 255, 0)

    @classmethod
    def rgb_to_hex(rgb):
        # should return hex
        return None

    @classmethod
    def hex_to_rgb(hex_code):
        # should return rbg
        return None

    @classmethod
    def cast_color(color):
        return (int(color[0]), int(color[1]), int(color[2]))


COLOR = _ColorClass()
