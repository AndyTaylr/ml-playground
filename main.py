
import pygame
from config import SCREEN_WIDTH, SCREEN_HEIGHT, COLOR

from environments import Screeps, GridWorld

def main():
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    pygame.display.set_caption("ML Playground")

    env = GridWorld()

    running = True

    while running:
        screen.fill(COLOR.BLACK)

        env.render(screen)

        action = None
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    running = False
                elif event.key == pygame.K_UP:
                    action = 0
                elif event.key == pygame.K_DOWN:
                    action = 1
                elif event.key == pygame.K_LEFT:
                    action = 2
                elif event.key == pygame.K_RIGHT:
                    action = 3

        if action is not None:
            reward, observations, done = env.step(action)
            print(reward)

            if done:
                env.reset()

        pygame.display.update()


if __name__ == '__main__':
    main()
